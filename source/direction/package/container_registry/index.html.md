---
layout: markdown_page
title: "Category Direction - Container Registry"
description: "The GitLab Container Registry is a secure and private registry for Docker images built on open source software. Learn more!"
canonical_path: "/direction/package/container_registry/"
---

- TOC
{:toc}

## Container Registry

The GitLab Container Registry is a secure and private registry for Docker images. Built on open source software and completely integrated within GitLab.  Use GitLab CI/CD to create and publish branch/release specific images. Use the GitLab API to manage the registry across groups and projects. Use the user interface to discover and manage your team's images. GitLab will provide a Lovable container registry experience by being the single location for the entire DevOps Lifecycle, not just a portion of it. We will provide many of the features expected of a container registry, but without the weight and complexity of a single-point solution. 

If you have any feedback about what's working well for you or what you would like to see us improve, please reach out to me directly via [E-mail](mailto:trizzi@gitlab.com).

- [Maturity plan](#maturity-plan)
- [Issue list](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3AContainer+Registry)
- [Overall vision](https://about.gitlab.com/direction/ops/#package)
- [Research issues](https://gitlab.com/groups/gitlab-org/-/boards/1397751?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Apackage&label_name[]=Category%3AContainer%20Registry)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

GitLab's Container Registry is robust, and integrated into your CI/CD process. It meets the needs of most container-based application development teams. As a result, our highest priority for the Container Registry is to lower the cost of storage on behalf of our customers and for GitLab.com. Those costs tend to grow without sufficient container registry management. 

[gitlab-#2313](https://gitlab.com/groups/gitlab-org/-/epics/2313) will allow Administrators to run [garbage collection](https://docs.gitlab.com/ee/administration/packages/container_registry.html#container-registry-garbage-collection) without requiring any downtime or setting the registry to read-only mode. 

[gitlab-#2270](https://gitlab.com/groups/gitlab-org/-/epics/2270), expands the utility of the [Cleanup policies for tags](https://docs.gitlab.com/ee/user/packages/container_registry/#cleanup-policy). [gitlab-#244050](https://gitlab.com/gitlab-org/gitlab/-/issues/244050) will begin the phased rollout of the cleanup policies for all historical projects. 

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [Make the Container Registry Complete](https://gitlab.com/groups/gitlab-org/-/epics/2899), which includes links and expected timing for each issue.

## Competitive Landscape

Open source container registries such as [Docker Hub](https://hub.docker.com/) and Red Hat's [Quay](https://quay.io/) offer users a single location to build, analyze, and distribute their container images. Docker Hub recently introduced rate limits for pulls from Docker Hub. 

The primary reason people don’t use DockerHub is that they need a private registry and one that lives alongside their source code and pipelines. They like to be able to use pre-defined environment variables for cataloging and discovering images. Often DockerHub is used as a base image for a test, but if you are building an app, you will likely customize an image to fit your application and save it GitLab's private registry alongside your source code.

[Artifactory](https://jfrog.com/artifactory/) and [Nexus](https://www.sonatype.com/nexus-repository-sonatype) both offer support for building and deploying Docker images. Artifactory offers their container registry as part of their community edition as well. 

Artifactory integrates with several different [CI servers through dedicated plug-ins](https://www.jfrog.com/confluence/display/RTF/Build+Integration), including Jenkins and Azure DevOps, but does not yet support GitLab. However, you can still connect to your Artifactory repository from GitLab CI. Here is an example of how to [deploy Maven projects to Artifactory with GitLab CI/CD](https://docs.gitlab.com/ee/ci/examples/artifactory_and_gitlab/index.html).

GitHub has recently released an open beta of their [container registry](https://docs.github.com/en/packages/managing-container-images-with-github-container-registry). Currently, the GitHub Container Registry only supports Docker image formats. During the beta, storage and bandwidth are free. After the beta, you can expect each tier to come with an included amount of storage and data transfers. Once you pass those limits, you will pay $0.25 USD per GB of storage and $0.50 USD per GB of data transfer. One concern worth raising is that we don't see a way to programmatically delete images. Given the cost of storing images, this could be a concern for organizations that heavily use GitHub's registry. Another limitation is that they only support authentication using your Personal Access Token. This is not ideal for organizations that would like to avoid using individual-level credentials. With the GitLab Container Registry, you may use a PAT, Deploy, or Job token to authenticate to the registry. 

There are several nice features that they've included. One nice feature is that you can publish images to your namespace or your user account. We would like to create that same functionality via [gitlab-#241027](https://gitlab.com/gitlab-org/gitlab/-/issues/241027). Also, their user interface includes helpful metadata, such as how often it's downloaded and a readme. 

Amazon offers a fully-featured registry and plans to [add support for highly available, publicly hosted images](https://aws.amazon.com/blogs/containers/advice-for-customers-dealing-with-docker-hub-rate-limits-and-a-coming-soon-announcement/). 

Google Cloud offers a container registry that allows you to integrate with any CI/CD platform. The registry is free, although they do charge for [storage and network egress](https://cloud.google.com/storage/pricing/). Google's registry includes container scanning and high availability.

JetBrains offers a [container registry](https://www.jetbrains.com/help/space/container-registry.html) that allows you to add a project repository and publish images and tags using the Docker client or your JetBrains project. Although they do not currently have any documentation for administrative features, such as cleanup policies or garbage collection.

[Digital Ocean offers a container registry](https://www.digitalocean.com/docs/container-registry/) that allows you store and configure private Docker images. In addition, they support global load balancing and caching in multiple regions. One potential drawback is that each Digital Ocean account is limited to 1 registry, whereas with GitLab each Project can have its own registry. 

## Top Customer Success/Sales Issue(s)

The top Customer Success/Sales issue is [gitlab-#196124](https://gitlab.com/gitlab-org/gitlab/-/issues/196124), which will enable support of the cleanup policies for all projects. 


## Top Customer Issue(s)

The top customer issue is [gitlab-2313](https://gitlab.com/groups/gitlab-org/-/epics/2313), which will remove the requirement for down time and unblock all of our customers (and GitLab) from running garbage collection. 


## Top Internal Customer Issue(s)

The top internal customer issue is tied to storage optimization. [gitlab-#2313](https://gitlab.com/groups/gitlab-org/-/epics/2313) will allow the Infrastructure team to lower the total cost of the GitLab.com Container Registry by implementing online garbage collection and removal of blobs.

Once we complete the above, [gitlab-#31039](https://gitlab.com/gitlab-org/gitlab/-/issues/31039) will begin to count registry storage towards project storage. This will be critical in rolling out consupmption storage pricing and reducing the cost of GitLab.com. 

## Top Vision Item(s)

Our top vision item is the epic [gitlab-#4946](https://gitlab.com/groups/gitlab-org/-/epics/4946) proposes making the registry a tier-zero service. This will help customers that want to rely on the registry for their production environments.
