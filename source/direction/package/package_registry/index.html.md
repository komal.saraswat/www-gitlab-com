---
layout: markdown_page
title: "Category Direction - Package Registry"
description: "As part of our overall vision for packaging at GitLab, we want to provide a single interface for managing dependencies, registries, and package repositories."
canonical_path: "/direction/package/package_registry/"
---

- TOC
{:toc}

## Package Registry

Our goal is for you to rely on GitLab as a universal package manager, so that you can reduce costs and drive operational efficiencies. The backbone of this category is your ability to easily publish and install packages, no matter where they are hosted. 

You can view the list of supported and planned formats in our documentation [here](https://docs.gitlab.com/ee/user/packages/).

- [Maturity plan](#maturity-plan)
- [Issue list](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3APackage+Registry)
- [Overall vision](https://about.gitlab.com/direction/ops/#package)
- [Research issues](https://gitlab.com/groups/gitlab-org/-/boards/1397751?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Apackage&label_name[]=Category%3APackage%20Registry)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

Since moving the Package Registry to Core, we've seen increased interest in and adoption of the registry. With that adoption, we've seen an increase in bugs and user experience issues. So, we are working through a list of issues to ensure the registry works reliability and seamlessly. 

[gitlab-#36423](https://gitlab.com/gitlab-org/gitlab/-/issues/36423) will give you the ability to publish and install NuGet packages using your group or sub-group. 

[gitlab-#276882](https://gitlab.com/gitlab-org/gitlab/-/issues/276882) introduces a new group setting that will allow you to choose whether or not to allow or disallow duplicate Maven or Gradle uploads. We will be rolling out this setting for each package manager format in the coming milestones. 

[gitlab-#216517](https://gitlab.com/gitlab-org/gitlab/-/issues/216517) is a technical investigation issue, but it's worth sharing. We'll be investigating adding support for adding RubyGems to the Package Registry. Supporting RubyGems would be valuable for many of customers, but also presents a great opportunity for GitLab to dogfood the registry. 

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is "Complete" (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [gitlab-#2891](https://gitlab.com/groups/gitlab-org/-/epics/2891), which includes links and expected timing for each issue.

## Competitive Landscape

### Universal package management tools
Artifactory and Nexus are the two leading universal package manager applications on the market. They both offer products that support the most common formats and additional security and compliance features. A critical gap between those two products and GitLab's Package offering is the ability to easily connect to and group external, remote registries. To date, GitLab has been focused on delivering Project and Group-level private package registries for the most commonly used formats. We plan on bridging this gap by expanding the Dependency Proxy to support remote and virtual registries. 

### Cloud providers
Azure and AWS both offer support for hosted and remote registries for a limited amount of formats. Google has a product called Artifact Registry that is in Alpha and supports Java and Node. All of the cloud providers charge for Cloud storage and network egress. 

### GitHub
GitHub offers a package management solution as well. They offer project-level package registries for a variety of formats. Looking at [GitHub's roadmap](https://github.com/github/roadmap/projects/1), it's clear that they intend to invest in package management. A few highlights:

- On their roadmap for Q4 2020 and Q1 2021 they will add support for GitHub Enterprise and GitHub private instances. 
- They are planning on adding more granular permissions for GitHub Packages in Q1 2021.
- They have implementations with Composer and PyPI planned but not scheduled. 
- They have a roadmap item for supporting generic packages. But, it's not yet scheduled. 

GitHub charges for storage and network transfers. GitHub does a nice job with search and reporting usage data on how many times a given package has been downloaded. They do not have anything on their roadmap about supporting remote and virtual registries, which would allow them to group registries behind a single URL and allow them to act as a universal package manager, like Artifactory or Nexus or GitLab.

### Supported formats

The below table lists our supported and most frequently requested package manager formats. Artifactory and Nexus both support a longer list of formats, but we have not heard many requests from our customers for these formats. If you'd like to suggest we consider a new format, please open an issue [here](https://gitlab.com/gitlab-org/gitlab/-/issues). 

|         | GitLab | Artifactory | Nexus | GitHub | Azure Artifacts | AWS CodeArtifact | Google Artifact Registry
| ------- | ------ | ----------- | ----- | ------ | ------ | ------ |  ------ |
| Composer    | ✔️ | ✔️ | ✔️️️️ | - | - | - | - |
| Conan       | ✔️ | ✔️ | ☑️ | - | - | - | - |
| Debian      | - | ✔️ | ✔️ | - | - | - | - |
| Gradle      | ✔️ | ✔️ | ✔️ | ️✔️ ️| ✔️ | ✔️ | ✔️ |
| Maven       | ✔️ | ✔️ | ✔️ | ️✔️ ️| ✔️ | ✔️ | ✔️ |
| NPM         | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ |
| NuGet       | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | - | - |
| PyPI        | ✔️ | ✔️ | ✔️ | - | ✔️ | ✔️ | - |
| RPM         | - | ✔️ | ✔️ | - | - | - | - |
| RubyGems    | - | ✔️ | ✔️ | ✔️ | - | - | - |


☑️ _indicates support is through community plugin or beta feature_

Interested in contributing a new format? Please check out our [suggested contributions](https://docs.gitlab.com/ee/user/packages/#suggested-contributions).

## Top Customer Success/Sales Issue(s)

- Our top customer success and sales issues are all focused on adding support for new package manager formats:
  - [gitlab-#803](https://gitlab.com/gitlab-org/gitlab/issues/803): RubyGems
  - [gitlab-#5835](https://gitlab.com/gitlab-org/gitlab/issues/5835): Debian, there is a community contribution in progress for this issue. 
  - [gitlab-#5932](https://gitlab.com/gitlab-org/gitlab/issues/5932): RPM

## Top Customer Issue(s)

- [gitlab-#36425](https://gitlab.com/gitlab-org/gitlab/-/issues/36425) will allow you to use an Instance-level endpoint for your NuGet packages. 

## Top Internal Customer Issue(s)

- The GitLab Frontend team would like to leverage the NPM Registry. The epic [gitlab-#3608](https://gitlab.com/groups/gitlab-org/-/epics/3608) will add support for caching dependencies and allow the GitLab frontend team to dogfood the feature. 
- The Distribution team utilizes an external tool for building Linux packages. By offering support for Debian and RPM we can begin to remove that external dependency.
- The GitLab Distribution team also utilizes RubyGems for downloading external dependencies. They would like to speed up their build times and remove their reliance on external dependencies by caching frequently used packages. This would require an integration with RubyGems as well as the dependency proxy. [gitlab-#225](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/225) details their needs and requirements.
- The Release group would like to leverage the GitLab Package Registry to store release assets and make them available for discovery and download. Although we are still working through how to best implement this, [gitlab-#36133](https://gitlab.com/gitlab-org/gitlab/issues/36133) discusses the need and use cases.

## Top Vision Item(s)

[gitlab-#2614](https://gitlab.com/groups/gitlab-org/-/epics/2614), captures our plan to improve our existing integrations by expanding our existing product to include the creation, management and usage of **remote** and **virtual** repositories.  
