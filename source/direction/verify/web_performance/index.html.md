---
layout: markdown_page
title: "Category Direction - Web Performance"
description: "Ensure performance of your software in-browser using automated web performance testing. Learn more here!"
canonical_path: "/direction/verify/web_performance/"
---

- TOC
{:toc}

## Web Performance

Ensure performance of your software in-browser using automated web performance testing. Our vision for Web Performance testing is to provide actionable performance data for most developers against top browsers in 5 minutes or less.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWeb%20Performance)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWeb%20Performance) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

Up next for Web Performance is [gitlab#10585](https://gitlab.com/gitlab-org/gitlab/issues/10585) which automatically runs browser performance testing on changed pages so developers can quickly understand any browser performance degradation from changes they have introduced without having to run a scan of the entire site or project.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and our next maturity target is "Viable" (see our [definitions of maturity levels](/direction/maturity/)). Key deliverables to achieve this are:

- [CI View for detailed site speed metrics](https://gitlab.com/gitlab-org/gitlab/issues/9878)
- [Add link to detailed sitespeed report from Merge Request](https://gitlab.com/gitlab-org/gitlab/issues/9879)

## Competitive Landscape

While, just as one could orchestrate any number of web browser testing tools with GitLab CI today, Jenkins, Travis or CircleCI could be used to orchestrate web browser testing tools in similar ways. None of these offer an out of the box web performance option but there are integrations available for them for top tools such as Google's Lighthouse and sitespeed.io.

In order to remain ahead of the competition we should continue to make GitLab a rich interface for Web Performance testing data for all contributors and expand beyond the current focus on the developer needs in a merge request. The Vision Items for the category reflect this direction.

## Top Customer Success/Sales Issue(s)

The Field teams are typically most interested in uptier features, like [Premium](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWeb%20Performance&label_name[]=GitLab%20Premium) and [Ultimate](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWeb%20Performance&label_name[]=GitLab%20Ultimate). The top feature in these categories include [CI View for detailed site speed metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/9878), [Provide Web Performance testing for high latency or low bandwidth network situations](https://gitlab.com/gitlab-org/gitlab/-/issues/9737), and [connecting sitespeed reports to the Merge Request](https://gitlab.com/gitlab-org/gitlab/-/issues/9879). 

## Top Customer Issue(s)

The most popular issue to date is [gitlab#9878](https://gitlab.com/gitlab-org/gitlab/issues/9878) which provides a more detailed view of the resulting report in the GitLab interface. We are actively seeking additional input from customers on how they are using the web performance data already available today before tackling this issue.

## Top Internal Customer Issue(s)

The Top Internal Customer issue is to have a [Project level report of Browser Performance Results](https://gitlab.com/gitlab-org/gitlab/-/issues/238049) so the quality team can use the existing feature and stop maintaining their own custom job to run browser performance testing against the nightly builds to create the [nightly report](https://gitlab.com/gitlab-org/quality/performance/-/wikis/Benchmarks/SiteSpeed/10k).

## Top Vision Item(s)

When we think out further about Web Performance there are opportunities to solve problems for customers like needing to [track web performance](https://gitlab.com/gitlab-org/gitlab/issues/36087) over time or finding performance issues sooner by [automatically running Browser Performance testing on changed pages](https://gitlab.com/gitlab-org/gitlab/issues/10585).

Beyond that we will look into opportunities to [display performance results by browser](https://gitlab.com/gitlab-org/gitlab/issues/121551) and tuning testing based on [real site metrics](https://gitlab.com/gitlab-org/gitlab/issues/121552). 
