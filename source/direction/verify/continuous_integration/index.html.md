---
layout: markdown_page
title: "Category Direction - Continuous Integration"
description: "Continuous Integration is an important part of any software development pipeline. It must be easy to use, reliable, and accurate. Learn more here!"
canonical_path: "/direction/verify/continuous_integration/"
---

- TOC
{:toc}

## Continuous Integration

Continuous Integration (CI) is an important part of any software development cycle, and defined as part of the [Verify stage](/direction/ops/#verify) here at GitLab. CI must be easy to use, reliable, and accurate in terms of results, so that's the core of where we focus. While we are very proud that we are recognized as [the leading CI/CD tool on the market](/blog/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/), as well as a leader in the 2019 Q3 [Cloud Native CI Wave](/resources/forrester-wave-cloudnative-ci/), it's important for us that we continue to innovate in this area and provide not just a "good enough" solution, but a great one.

As we continue to improve the experience of running a pipeline, we also want to help you better analyze your pipelines. Making it easy to run a pipeline is our first focus and this applies to both running a pipeline manually as well as triggering one automatically when submitting a code commit or a merge request. In addition, we want to provide data for examining your pipeline's performance, so that you can optimize CI configurations to make your pipelines run more efficiently.

## Additional Resources

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration)
- [Overall Vision of the Verify stage](/direction/ops/#verify)
- [JTBD Overview](https://about.gitlab.com/handbook/engineering/development/ops/verify/continuous-integration/jtbd/)

For specific information and features related to authoring/defining pipelines, check out [Pipeline Authoring](/direction/verify/pipeline_authoring). You may also be looking for one of the following related product direction pages: [GitLab Runner](/direction/verify/runner/), [Continuous Delivery](/direction/release/continuous_delivery/), [Release stage](/direction/ops#release), or [Jenkins Importer](/direction/verify/jenkins_importer).

## What's Next & Why

In [gitlab#30101](https://gitlab.com/gitlab-org/gitlab/-/issues/30101), we are making it easier to run a pipeline by pre-filling the variables on the Run Pipeline form. This eliminates the need to manually enter all the required key/value pairs of variables necessary to successfully trigger a pipeline. 

Keeping the latest artifact for a successful pipeline may not always be needed, so we are implementing the ability to configure this at a project-level [gitlab#241026](https://gitlab.com/gitlab-org/gitlab/-/issues/241026) and also at an instance-level (for self-managed instances)[gitlab#276583](https://gitlab.com/gitlab-org/gitlab/-/issues/276583). And if you need to do some "cleanup" of artifacts already stored, we are working to deliver an API to let you quickly delete artifacts in bulk [gitlab#223793](https://gitlab.com/gitlab-org/gitlab/-/issues/223793).

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our [definitions of maturity levels](/direction/maturity/)), it is important to us that we defend that position in the market. As such, we are balancing prioritization of [important P2 issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=priority%3A%3A2) and [items from our backlog of popular smaller feature requests](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Acontinuous+integration&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93) in addition to delivering new features that move our vision forward. 

To keep Continuous Integration lovable, we have a 1-year focus via [gitlab&4794](https://gitlab.com/groups/gitlab-org/-/epics/4794). Some of the top features include: 

- [Present information around running pipeline activities in GitLab CI](https://gitlab.com/groups/gitlab-org/-/epics/5071)
- [Improve the experience around debugging jobs and analysing pipeline](https://gitlab.com/groups/gitlab-org/-/epics/5022)

## Competitive Landscape

The majority of CI market conversation is between us, Jenkins, and GitHub Actions at this point. Atlassian has built BitBucket Pipelines, a more modernized version of Bamboo, which is still in the early stages. Microsoft is maintaining (at least for now) Azure DevOps at the same time as GitHub. CodeFresh and CircleCI have both released [container-based plugin model](https://steps.codefresh.io/), similar to GitHub Actions. CircleCI in particular is known for very fast startup times and we're looking to ensure we [keep up or get even faster](https://gitlab.com/groups/gitlab-org/-/epics/439). Jenkins is largely seen as a legacy tool, and most people we speak with are interested in moving off to something more modern. We are addressing this with our [Jenkins Importer](/direction/verify/jenkins_importer) category which is designed to make this as easy as possible.

From [GitHub's 2021 Roadmap](https://github.com/github/roadmap/projects/1), we are seeing GitLab-reminiscent features which include [Auto-merge](https://github.com/github/roadmap/issues/107), akin to [Merge Trains](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/) with a fit-finish that we aim to make easier in [gitlab#294169](https://gitlab.com/gitlab-org/gitlab/-/issues/294169). Also to note is an emphasis on governance and controls with [Action Events in Audit Logs](https://github.com/github/roadmap/issues/109), bringing about a sense of integration we are creating with the [Compliance group's Audit Events](https://docs.gitlab.com/ee/administration/audit_events.html).

## Analyst Landscape

There are a few key findings from the Forrester Research analysts on our CI solution. GitLab is seen as capable as the solutions provided by the hyperclouds themselves, and well ahead of other neutral solutions. This can give our users flexibility when it comes to which cloud provider(s) they want to use. We are also seen as the best end to end leader, with other products  not keeping up and not providing as comprehensive solutions. What this tells us is that it is important for us to continue to innovate and make it hard or even impossible for competitors to maintain pace.

As such, our path to improving our analyst performance matches our solutions above in terms of staying ahead of our competitors.

## Top Customer Success/Sales Issue(s)

The most popular Customer Success issues as determined in FQ1-20 survey of the Technical Account Managers was [filtering pipelines by status, branch or trigger source](https://gitlab.com/groups/gitlab-org/-/epics/3286). Also important for the sales team is [gitlab#205494](https://gitlab.com/gitlab-org/gitlab/issues/205494) which will allow for easier use of GitLab's security features when not using GitLab's CI.

## Top Customer Issue(s)

Our top customer issues ([search results](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=customer)) include the following:

- [Ensure after_script is called for cancelled and timed out pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/15603)
- [Generate Run Pipeline form with pre-filled variables from .gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab/-/issues/30101)
- [Support multiple artifacts generation per job](https://gitlab.com/gitlab-org/gitlab/-/issues/18744)
- [Introduce API endpoint for cleaning artifacts](https://gitlab.com/gitlab-org/gitlab/-/issues/14495)

Another item with a lot of attention is to normalize job tokens in a more flexible way, so that they can have powerful abilities when needed and still not introduce security risks ([gitlab#3559](https://gitlab.com/groups/gitlab-org/-/epics/3559)).

We also have a few issues about making variables available before includes are processed, however there is a "chicken and egg" problem here that has been difficult to solve. Child/parent pipelines solves some use cases, but not all, and in the meantime we are continuing the discussion in the issue [gitlab#1809](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809). If you're interested in technical discussion around the challenges and want to participate in solving them, please see the conversation [here](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809#note_225636231). There are two related epics here, [Use a variable inside other variables in .gitlab-ci.yml](https://gitlab.com/groups/gitlab-org/-/epics/3589) and [Raw (unexpanded) variables MVC](https://gitlab.com/groups/gitlab-org/-/epics/1994)

## Top Internal Customer Issue(s)

Our top internal customer issues ([search results](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=internal%20customer)) include the following:

- [Recognise links to urls and repository files in job logs](https://gitlab.com/gitlab-org/gitlab/-/issues/18324)
- [Retry manual job with different variables](https://gitlab.com/gitlab-org/gitlab/-/issues/32712)

Our top dogfooding issues ([search results](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=Dogfooding)) are:

- [Group level pipeline dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/11960)
- [Public pipeline page MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/10861)
- [Expand pipeline dashboard to contain stages](https://gitlab.com/gitlab-org/gitlab/-/issues/11959)

## Top Vision Item(s)

Looking to the future, we have plans to help you better monitor and understand your pipeline [epic#4794](https://gitlab.com/groups/gitlab-org/-/epics/4794). Having details about pipeline activities (such as job duration) will allow you to see and react to what's happening while your pipeline is running. Beyond using data simply for reactive purposes, we have plans for a customizable UI for historical pipelines analytics so you can see the trends that will guide your planning and decision making.

Even further into the future, we are looking to expand insights and predications of CI use to help you reduce waste in our 3-year vision via [gitlab&4793](https://gitlab.com/groups/gitlab-org/-/epics/4793).
