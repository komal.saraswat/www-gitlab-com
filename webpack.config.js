// webpack.config.js
const webpack = require('webpack');
const path = require('path');
const glob = require('glob');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");
const CopyPlugin = require("copy-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');

// Captures all scss files
const mapStylesheets = pattern => glob
  .sync(pattern)
  .reduce((entries, filename) => {
    const [, name] = filename.match(/([^/]+)\.scss$/)
    return { ...entries, [name]: filename }
  }, {})

const mapJavascripts = pattern => glob
  .sync(pattern)
  .reduce((entries, filename) => {
    const [, name] = filename.match(/([^/]+)\.js$/)
    return { ...entries, [name]: filename }
  }, {})

module.exports = {
  // Catch all files within javascript and stylesheet directory
  entry: {
    ...mapStylesheets('./source/stylesheets/*.*.scss'),
    ...mapJavascripts('./source/javascripts/*.js'), // this catches all js files in /source/javascripts, output to source/javascripts/
    'applications/broken_image_placeholder': './source/javascripts/applications/broken_image_placeholder.js',// Start  catch of source/javascripts/applications/
    'applications/categories_scroller': './source/javascripts/applications/categories_scroller.js',// will output to  be   ./dist/javascripts/applications/ 
    'applications/search_bar': './source/javascripts/applications/search_bar.js',// END: catch of source/javascripts/applications/ 
  },

  // Output all javascript to specific directory

  output: {
    path: path.join(__dirname, '/tmp/dist'),
    filename: 'javascripts/[name].js',
  },

  resolve: {
    modules: [
      "node_modules"
    ]
  },


  module: {
    rules: [
      // Extracts the compiled CSS from the SASS files defined in the entry
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            // Interprets CSS
            loader: "css-loader",
            options: {
              importLoaders: 2
            }
          },
          {
            loader: 'sass-loader' // 将 Sass 编译成 CSS
          }
        ],
      }
    ],
  },

  plugins: [
    // Where the compiled SASS is saved to
    // Note: If [name].css is included in filename it outputs .css.css for css.scss files
    // Todo: [name] will not capture just filename.scss files such as blog-landing.css
    new MiniCssExtractPlugin({
      filename: "stylesheets/[name]",
      chunkFilename: "stylesheets/[name].css"
    }),
    // Removes js files produced from scss compiling
    new FixStyleOnlyEntriesPlugin(),
    // Just copy the javascript libraries for now because we're not bundling them.
    new CopyPlugin({
      patterns: [
        {
          from: "libs/*.js",
          to: "javascripts/",
          context: "source/javascripts",
        },
        {
          from: "maturity.js",
          to: "javascripts/applications",
          context: "source/javascripts",
        },
        {

          from: "bl-modal.js",
          to: "javascripts/applications",
          context: "source/javascripts",
        },
        {
          from: "countdown.js",
          to: "javascripts/applications",
          context: "source/javascripts",
        },
        {
          from: "guide-to-the-cloud.js",
          to: "javascripts/applications",
          context: "source/javascripts",
        },
        {
          from: "image-lazy-load.js",
          to: "javascripts/applications",
          context: "source/javascripts",
        },
        {
          from: "run-experiment.js",
          to: "javascripts/applications",
          context: "source/javascripts",
        },
      ],
    }),
  ],
  // Compresses javascript files
  optimization: {
    minimizer: [
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          safe: true
        }
      }),
      new TerserPlugin()
    ]
  },

  stats: {
    entrypoints: false,
    children: false
  },
};