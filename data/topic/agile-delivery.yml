file_name: agile-delivery
title: Agile delivery
description: Agile delivery is an iterative approach to software delivery in
  which teams build software incrementally at the beginning of a project rather
  than ship it at once upon completion.
canonical_path: /topics/agile-delivery/
twitter_image: /images/topics/ci-cd-opengraph.png
header_body: >-
  Agile delivery is an iterative approach to software delivery in which teams
  build software incrementally at the beginning of a project rather than ship it
  at once upon completion.


  [Learn how to accelerate delivery →](https://learn.gitlab.com/c/201906-whitepaper-re?x=9tzwq4&utm_medium=other&utm_source=webpage&utm_campaign=betterproductsfaster)
body: >
  ## What is Agile delivery?


  Agile is one of the most important and transformative methodologies introduced to the software engineering discipline in recent decades, helping software teams efficiently create customer-centric products.


  > Agile delivery is an iterative approach to software delivery in which teams build software incrementally at the beginning of a project rather than ship it at once upon completion.


  Agile development means taking iterative, incremental, and lean approaches to streamline and accelerate the delivery of projects.


  ## Why embrace Agile delivery?


  The demand for faster software development is universal, and Agile delivery meets both customer and business needs.


  Organizations that adopt Agile delivery practices can gain a competitive edge in a fast changing market. Businesses that empower teams to use Agile development practices satisfy discerning customers and adapt to new technologies, helping them to develop the products that set the standard for industries.


  It's not just businesses that benefit from Agile delivery. Customers have more substantive experiences with organizations when their needs are met and their feedback makes a difference in product development. Customers appreciate when their input and expectations help shape an organization’s releases.
cover_image: /images/topics/agile.svg
benefits_title: Agile delivery basics and benefits
benefits_description: Getting started with Agile means becoming acquainted with
  the most common methodologies and characteristics.
benefits:
  - title: Scrum
    description: Scrum, often synonymous with Agile, is an approach that emphasizes
      continuous improvement, self organization, and experience-based learning.
      By utilizing user stories, tasks, backlogs, and extensions, teams have a
      structured model to carry them across a software development lifecycle.
      Teams that use a Scrum approach to development are likely to be committed,
      respectful, and focused.
    image: /images/icons/scrum.svg
  - title: Kanban
    description: "Teams that use a Kanban framework favor transparency and
      communication. Tasks are organized using Kanban cards on a board to enable
      end-to-end visibility throughout production. Three practices guide Kanban:
      visualize work, limit work in progress, manage flow. Teams that use a
      Kanban framework are collaborative, transparent, balanced, and customer
      focused."
    image: /images/icons/kanban.svg
  - title: Agile mindset
    description: An Agile mindset means viewing setbacks as learning opportunities,
      embracing iteration, collaboration, and change, and focusing on delivering
      value. With an Agile mindset, teams can adjust to changing market needs,
      respond to customer feedback, and deliver business value. Adopting a new
      perspective can positively change a team’s culture, since the shift
      permits innovation without fear, collaboration with ease, and delivery
      without roadblocks.
    image: /images/icons/agile.svg
  - title: Speed to market
    description: Faster time-to-market enables quicker customer feedback and higher
      customer satisfaction.
    image: /images/icons/scale.svg
  - title: Higher quality
    description: Since testing is integrated throughout the lifecycle, teams have
      early sight into quality issues.
    image: /images/icons/computer-test2.svg
  - title: Transparency
    description: Teams are involved throughout a project — from planning and
      prioritizing to building and deploying.
    image: /images/icons/visibility.svg
suggested_content:
  - url: /blog/2018/03/05/gitlab-for-agile-software-development/
  - url: /blog/2019/06/13/agile-mindset/
  - url: /blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/
  - url: /blog/2019/06/20/issue-labels-can-now-be-scoped/
  - url: /blog/2019/06/10/manage-conversation-staying-agile/
  - url: /blog/2019/08/20/agile-pairing-sessions/
resources_title: Resources
resources_intro: >-
  Here’s a list of resources on Agile that we find to be particularly helpful in
  understanding Agile and implementation. We would love to get your
  recommendations on books, blogs, videos, podcasts and other resources that
  tell a great Agile story or offer valuable insight on the definition or
  implementation of the practice.


  Please share your favorites with us by tweeting us [@GitLab](https://twitter.com/gitlab)!
resources:
  - title: Agile project management
    url: https://www.youtube.com/watch?v=wmtZKC8m2ew
    type: webcast
  - title: Setting up Agile groups and teams
    url: https://www.youtube.com/watch?v=VR2r1TJCDew
    type: webcast
  - title: Scaled Agile Framework (SAFe) with GitLab
    url: https://www.youtube.com/watch?v=PmFFlTH2DQk
    type: webcast
  - title: Agile planning
    url: /solutions/agile-delivery/
    type: Reports
  - title: Scaled Agile and GitLab
    url: https://about.gitlab.com/solutions/agile-delivery/scaled-agile/
    type: Reports
  - title: Accelerating software delivery
    url: /solutions/agile-delivery/scaled-agile/
    type: Reports
  - url: /customers/axway-devops/
    title: Axway was able to achieve hourly deployments with GitLab CI/CD
    type: Case studies
  - title: How GitLab CI supported Ticketmaster's ramp up to weekly mobile releases
    url: /blog/2017/06/07/continous-integration-ticketmaster/
    type: Case studies
