---
layout: handbook-page-toc
title: "How the UX Research team operates at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## How we decide what to research
UX Researchers collaborate with Product Managers to determine the scope of research studies. Where possible, UX Researchers should try to attend planning meetings for their designated groups.
UX Researchers should proactively offer ways in which they can assist in the delivery of research. They should also suggest and discuss their own ideas for research studies with Product Managers.

## How we spend our time
UX Researchers have the following guidance on how they should be spending their time:

* **<10% Solution Validation** - This translates to less than 10% of a researcher's time being allocated to assisting Product Designers and Product Design Managers with Solution Validation research.
* **~60% Problem Validation** - Researchers spend more than half of their time working with Product Managers conducting Problem Validation research, with the long-term goal of investing their time towards training and mentoring.
* **~30% Foundational/Strategic Research** - Ideally, 1 big project per researcher, per quarter. These are projects that feed into the researcher's stage/team and may be derived by the researcher. Examples:
    * Go deeper into a SUS theme that is specific to the stage(s) they support. Doing this will help the team focus on issues that contribute to detractors.
    * Look deeper into performance (ex: Why is our performance perceived the way it is?)
    * Pain points - Are those known for each stage and/or group? Are they being dealt with? Are there real-life customer stories to help get those pain points addressed?
    * A project could even be derived from a Problem Validation topic or follow-up area.

Three noteworthy benefits to conducting Foundational/Strategic research:
1. Researchers gain subject matter knowledge in that topic
1. Researchers have an opportunity to impact Product and influence strategy
1. Career growth opportunities

## Metrics we measure
As user researchers, we leverage different types of metrics to aid in measuring the success of our product. We currently use metrics to evaluate [navigation changes](/handbook/engineering/ux/ux-research-training/evaluating-navigation/). 

## Milestones
Like other departments at GitLab, UX Researchers follow the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) and use milestones to schedule their work. Milestones change monthly ([find out the dates for upcoming milestones](https://gitlab.com/groups/gitlab-org/-/milestones)).

## How to request research
Any GitLab Team Member can open a research request. If you are **not** a Product Manager, Product Designer, or UX Researcher, please open a Research request issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/). Once completed, please assign the issue to the relevant [Product Manager](/handbook/product/categories/), [Product Designer](/handbook/product/categories/), and [UX Researcher](https://about.gitlab.com/company/team/?department=ux-research-team). The UX researcher assigned to the issue  will review it and notify you when/if they plan to proceed with the work.

## How we work
1. Assign yourself to any research issues you are leading. It is optional to assign yourself to any research issues you are supporting.
1. Proactively contact your [Product Director](/handbook/product/categories/) and request their input to prioritize the existing UX research issues in your stage. Inform your Product Director of your capacity for research during the upcoming two or three milestones. This may be more effective as a Zoom call at first, and then you can transition to an async conversation in an issue.

#### How problem validation research works
* [Problem Validation research for single-stage-group initiatives](/handbook/engineering/ux/ux-research-training/problem-validation-single-stage-group/)
* [Problem Validation research for multi-stage-group initiatives](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/problem-validation-multi-stage-group/) 

#### How solution validation research works
Solution validation research at GitLab is led by Product Designers, with support from Product Design Managers. Occasionally, Product Design Managers may need to escalate queries about solution validation research to UX Researchers for advice and feedback. 

If capacity allows, UX Researchers can help with solution validation research efforts.
Product Managers and Product Designers follow the steps in the [Validation phase 4](/handbook/product-development-flow/#validation-phase-4-solution-validation) when planning and executing solution validation research. 


