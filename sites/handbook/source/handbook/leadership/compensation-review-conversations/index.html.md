---
layout: handbook-page-toc
title: Compensation Review Conversations
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Compensation Review Conversations
Conversations with regards to compensation are an important part of managing. This page will take you through information and recommendations. However if you're ever in doubt or have a question don't hesitate to reach out to your [People Business Partner.](/handbook/people-group/#people-business-partner-alignment-to-division). 

### Compensation Review cycles

Check the most up to date information with regards to our [Annual Compensation Review cycle](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review-timeline) and our [Targeted Mid-Year Increase Review](/handbook/total-rewards/compensation/compensation-review-cycle/#targeted-mid-year-increase-review) on the [Total Rewards page](/handbook/total-rewards/compensation/compensation-review-cycle).

Starting the FY22 Annual Compensation Review Total rewards will be providing a letter of adjustment outlining the new and former compensation as well as unvested equity value. We recommend to review this letter before the conversation and use the letter as preparation to communicate exact figures.  

### Communication recommendations

* **Communicate the increase face-to-face over [Zoom](/handbook/communication/#zoom).** As a manager, this is an opportunity for you to have a conversation with your team member about their increase and the reason behind it.  Having the conversation over Zoom allows for you to have a dialogue with your team member (versus just sharing the number or percentage) and allows you to pick up other information like tone and non-verbal cues which can tell you more about how someone is feeling.

* **Prepare for the call ahead of time.** As a manager, you should have awareness of the following facts about GitLab's compensation principles (please review the handbook's [Global Compensation](/handbook/total-rewards/compensation/) page), any recent changes to the compensation calculator/band and personal details about your team member in BambooHR:
    * Hire date
    * Current compensation (including base salary, variable (where applicable), etc.)
    * Date of last compensation adjustment
    * Performance Factor
    * Location factor changes
    * Benchmark changes
    * % Increase: Please calculate this number from BambooHR to at least the hundredth place rather than using the rounded version available in Compaas, `((FY22 Salary-FY21 Salary)/FY21 Salary)`

* Communicate the adjustment at the beginning of the meeting. You want to give the team member time to ask questions and discuss their compensation adjustment. Avoid trying to rush to communicate at the end of a [1:1 meeting](/handbook/leadership/1-1/).

* Try to clearly explain what brought you to a certain increase. The preparation for the [Performance factor conversation](/handbook/people-group/performance-assessments-and-succession-planning/#best-practices-for-communicating-performance-factors) could possibly help there too. 

* Protect the confidentiality of other team members by avoiding saying things like “everyone else received less than you” or “you were the only team member to get a discretionary increase.”

* Avoid blaming others (For example: “I would have given you more but management didn’t approve.”)

* Avoid making future promises (For example: “In the next review, I will get you a large adjustment.”)

### Scenarios (situations and reactions)

**Situations:**

1. **Receiving an increase:**  A team member is receiving an adjustment based on Performance Factor AND/OR location factor/Geo Region AND/OR benchmark changes AND/OR Discretionary:
    * Sample Script:
    * I am so pleased to inform you that you will be getting an adjustment in this year’s comp review. Your increase is based on one or more the following factors (Performance, Location Factor/Geo Region, Benchmark change and/or Discretionary).  
    * Your salary will be increased to (insert $ amt in local currency) which is a (insert %) adjustment effective, February 1st.
    * Thank you for your hard work and I look forward to continuing to work with you! Do you have any questions?

2. **Not receiving an increase:**   A team member is not receiving an adjustment:
    * Sample Script:
    * As you know, GitLab just went through our Annual Compensation Review. I wanted to inform you that you are not getting a compensation adjustment at this time. This is due to (examples below)...
        * A team member is new to the team and at the market range for your aligned role.  *Your performance is good (if applicable), however, we hire new team members at market rate for compensation so we feel that you are compensated accurately for your role at this time. You may be eligible to participate in the the [Targeted Mid-year Increase Review process](/handbook/total-rewards/compensation/compensation-review-cycle/#targeted-mid-year-increase-review).*
        * A team member is not receiving because they are above range for their role.   *Your compensation is above the market pay range for your role and therefore you are not recieving an additional increase as a part of the Annual Compensation Review. Your performance is good (if applicable), so let's discuss what you want to work on in the future and create a [development plan](/handbook/people-group/learning-and-development/career-development/) together.*
        * A team member needs to improve performance. For questions on specific situations, please work with your People Business Partner.

    * Although informing a team member that they are not getting a compensation adjustment is a tough message to deliver, managers should have this direct conversation. This is directly aligned with our transparency value. We want everyone to know why they may not have received an adjustment and give them the space to ask questions.

3.  **Other scenarios:** If you have a scenario different from the above, and/or you need help with messaging, please work with your manager or [People Business Partner](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group).  

**Possible reactions:**

1. A team member is not happy with their adjustment.
    * Listen to your team member's feedback and allow them to express their concerns.
    * Ask open ended questions to allow for the team member to share more (like what were your expectations for an adjustment this year? Why were those your expectations?)
    * Reiterate GitLab’s compensation philosophy. We pay to market based on the compensation calculator and increase compensation annually based on performance.
    * Point your team member to the Compensation Calculator where they can see the range for their role.
    * If your team member is in good standing from a performance perspective, work with them to put together a [development plan](/handbook/people-group/learning-and-development/career-development/) to help them achieve their goals (e.g. skill development or a promotion).

2. A team member is not happy with their performance factor.
    * Listen to your team member’s feedback and allow them to express their concerns.
    * Refer back to the conversation you had with your team member at the time performance factors were determined and reiterate the justification and feedback that you provided at that time.
    * If your team member is in good standing from a performance perspective, work with them to put together a [development plan](/handbook/people-group/learning-and-development/career-development/) to help them continue to grow. 

**If you need assistance after reviewing the [handbook](/handbook/total-rewards/compensation/), please work directly with your manager or your [People Business Partner](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group)**
