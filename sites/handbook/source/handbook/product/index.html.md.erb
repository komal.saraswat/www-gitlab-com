---
layout: handbook-page-toc
title: Product Handbook
---

## Welcome to the Product Team Handbook

- Most of the content in the product handbook talks about _how_ we do product at GitLab.
- To learn more about _what_ we are currently building and our product direction see [Product Direction](/direction).
- Are you looking to talk to a Product Manager or collaborate with the Team see our [How to Engage](/handbook/product/how-to-engage) guide.

### Product Team Mission

**As a Product Org we strive to consistently create products and experiences that customers love and value.**

- **Consistency wins as you scale**. Our organizational goal is to create a Product Manager (PM) system that maximizes the chances of success across new products, new features, and new team members.
- **We are shipping an experience**, and not just a product. Don’t forget about the links between the product and the website, pricing & packaging, documentation, sales, support, etc.
- **It’s about our customers and doing a job for them**, not the product itself. Think externally about customer problems, not internally about the technology.
- **It’s about love AND value**. Will customers value what we are building? We need to make sure what we build helps build and extract customer value.

#### [**Product Principles**](/handbook/product/product-principles)

The Product Principles section is where you can learn about our strategy and philosophy regarding product development here at GitLab.

#### [**Product Processes**](/handbook/product/product-processes)

For a detailed view on how we do Product Development, read up on our established Product Processes.

#### [**Product Categorization**](/handbook/product/categories)

To learn how the GitLab product and our Teams are organized review our Product Categorization section.

#### [**About the GitLab Product**](/handbook/product/gitlab-the-product)

Learn about GitLab as a product, including what does it mean to be a single application, our subscription tiers and pricing model, and the basics of permissions in the platform.

#### [**Being a Product Manager at GitLab**](/handbook/product/product-manager-role)

Want to know more on what being a Product Manager at GitLab is like? Checkout our [Product Manager Role](/handbook/product/product-manager-role/) guide for helpful information like our Career Development Framework and the Roles and Responsibilities of a PM.

#### [**Product Performance Indicators**](/handbook/product/performance-indicators/)

Learn how we measure success in Product via our Product KPIs and OKRs, which are tracked in our [Product project](https://gitlab.com/gitlab-com/Product). For best practices and guidance on how to add instrumentation for features please review our [Product Intelligence workflow](/handbook/product/performance-indicators/#product-intelligence-workflow).

#### [**Our Product Leadership Team**](/handbook/product/product-leadership)

Learn about our Product Leadership Team and learn about them via their personal README’s.

#### Popular Product Resources

- [**Product Operations Releases**](/handbook/product/product-operations/)
- [**Product Development Flow**](/handbook/product-development-flow/)
- [**Product Development Timeline**](/handbook/engineering/workflow/#product-development-timeline)
- [**Product Intelligence Guide**](/handbook/product/product-intelligence-guide)
- [**Data for Product Managers**](/handbook/business-ops/data-team/data-for-product-managers/)
- [**Product Pricing Model**](/handbook/ceo/pricing/)
- [**GitLab Release Posts**](/handbook/marketing/blog/release-posts/)

### Communicating with the Product Team

GitLab team members should leverage [How to Engage](https://about.gitlab.com/handbook/product/how-to-engage/) to interact with the product team.

Members of the Product team are added to the [private product group](https://gitlab.com/gl-product) `@gl-product`. This group is used for internal communication and the `@gl-product` mention can only be used by project members.
Please remember that tagging `@gl-product` on issues will generate in-product [todos](https://docs.gitlab.com/ee/user/todos.html) and email notifications to **all** product team members, so use it only when you need to communicate with the entire product team.

If you are tagging `@gl-product`:

- Clearly state why you are tagging the entire product team and what action you need product team members to take.
- Write a short summary in the same comment so team members can quickly understand the necessary context.
- Review the issue title and description to ensure it has relevant details other product team members need **BEFORE** submitting the comment. The issue title will be the subject of email notifications and in-product todos.
- If asking team members to review a change, please directly link to the specific page on the [review app](https://docs.gitlab.com/ee/ci/review_apps/#how-review-apps-work) and any relevant issues or MRs.

##### Required Knowledge for Product Managers

<kbd class="required">Required 🔎</kbd>

The Product handbook contains a lot of content that includes a mix of advice, best-practices,
mandatory process, team-specific content, and more. To help you find the most important
items, look for the badge above that indicate the most essential, important, and/or
obligatory processes that every Product Manager is expected to know by heart and follow,
as opposed to other items which may be more for reference in certain situations or may
be more optional than required. If you are adding or updating an essential handbook section,
consider adding this badge there.

#### Adding to or updating the product handbook 

If you see any typos, copywriting improvements or small content clarifications you'd like to contribute, please create a merge request and merge it yourself (provided you have merge permissions to this repository). And please mention [Product Operations](/company/team/#fseifoddini) in the merge request as a heads up and so we can thank you! Since we have a [bias for action](/handbook/values/#bias-for-action), we trust your judgement.

If you have a larger change such as wanting to add new sections or significantly modify content in an existing section, please create a merge request and direct ping [Product Operations DRI](/company/team/#fseifoddini) to simply inform, request review or seek collaboration. If you have a larger change such as wanting to add/modify content to the product handbook landing page or adding entirely new pages (or don't have merge permissions), please create a merge request and direct ping [Product Operations DRI](/company/team/#fseifoddini) for review, collaboration and approval. If you're looking ot create an entirely new directory, it is recommended you discuss in advance with product operations to avoid needing to move it or resturcture it later. 

Please note it is your responsibility to inform GitLabbers as needed about your merge request, so we can all stay aligned and informed. Depending on the scope of changes, consider the following these best practices to inform product managers and other cross-functional team members:

- If specifically relevant to product management, tag '@gl-product' in the MR prior to merging
- If relevant to other teams, tag department leads such as the VP of UX, VP of Development or the Director of Quality Engineering in the MR prior to merging
- Share and cross post the MR link with a brief description in relevant channels such as Slack #product, #product-leadership, #eng-managers, #ux-managers
- Add as a read-only or discussion topic as appropriate in the the [Weekly Product Management Meeting](/handbook/product/product-processes/#weekly-product-management-meeting)
- If you need more guidance on how/where/when to share, check out more [tips and best practices](/handbook/communication/#top-tips-and-best-practices) in [GitLab Communication](/handbook/communication/)
