---
layout: markdown_page
title: "Eric Brinkman's README"
---

## Eric's README

Hey there, my name is Eric and I'm a product director here at GitLab. This README is meant to tell you a bit more about myself and to serve as a way for what it might be like to work with me.

I joined GitLab in 2018 and created this [README deck](https://docs.google.com/presentation/d/1uZQZ-UAPcQacKAfomIL_Znk3_OUAYODFuQgJtGxRyQY/edit#slide=id.g2823c3f9ca_0_30) during my first week. While some of it is focused on ramping up, most of the information in the deck still holds true.

## About me

- I grew up mainly in a small town in Iowa, along the banks of the Mississippi river. I was raised to value hard work, honesty, and kindness and I attempt to live out those precepts each and every day.
- I have been married for over 12 years and have three children. Family is incredibly important to me and I try hard to be the best father and husband I can be, though I fall short at times.
- I live in San Antonio, TX where I enjoy the warm weather, attractions like Six Flags and Sea World, smoking meats, reading, coaching little league, riding bikes, and playing the occasional video game.
- I attended the University of Illinois in Urbana-Champaign, and despite our track record of ending up in the bottom half of the standings nearly every year in both basketball and football, I watch most games on TV.
- I've got an interesting professional background that spans hardware, software, cloud, cybersecurity, dev tools, and DevOps, but I've always worked in the tech industry. For a full background, you can check out my [LinkedIn profile](https://www.linkedin.com/in/ericbrinkman/).
- My Barton StrengthsFinder top 5 strengths are:
    1. Analytical
    1. Belief
    1. Harmony
    1. Responsibility
    1. Maximizer
- My Meyers Briggs profile is ENTJ

## My role as a Senior Director of Product

As a Senior Director of Product at GitLab, it's my job to build and lead a high functioning team of product managers. The [job description](https://about.gitlab.com/job-families/product/director-of-product/) for product directors is well defined, however, there are a few responsibilities that are near and dear to me, related to building our team. I really get excited by:

- Sourcing, recruiting, and hiring the best talent we can find for any open roles we have.
- Ensuring fantastic onboarding experiences in order to ramp up team members as quickly as possible.
- Meeting with directs via regular 1x1s to build relationships, get to know team members deeply, and unblock them.
- Ensuring I am helping achieve career goals, and working with team members proactively to do so.
- Empowering team members to make the best decisions for their products by being a servant leader and removing any roadblocks I can.
- Serving team members by helping them work through tough issues.
- Collaborating with team members on important product strategy initiatives.

In June of 2020, I was promoted to Senior Director of Product. For more information on why I was promoted, check out my promotion [doc](https://docs.google.com/document/d/e/2PACX-1vTYv7raFcINAYQfiHpTHjqsZ7eSvlENL7MuKhKWVCoPRZLmcGS9vtg8siI8dzn_3KXdho9MR_NI3nT1/pub).

## Communicating and working with me

- I love getting to know people personally and deeply - if this is not your desired style/interaction, please let me know and I will adapt to you and your communication style!
- I love to work through problems together. Don’t feel like you need to always bring a solution with a problem, though it can be helpful if we have a proposal to discuss.
- I tend to listen first, and then contribute to conversations. I really love to gather as much context as possible. I would rather think about what you are saying, rather than wait for the next available space where I can say what I had planned to all along. I will even listen with respect to viewpoints that I disagree with. It's not always feasible to respond point by point to something I disagree with, so I will typically concentrate on the bigger picture. Please do not interpret my active listening or lack of point by point response as implicit agreement.
- I desire for decision making to be at the level closest to those who are most familiar with the subject are. I will make suggestions and ask probing questions, but at the end of the day, product managers are the directly responsible individuals for decision making and prioritization.
- I strive to work on harmonious teams in highly collaborative environments. I do not enjoy conflict, but will address it head on in the attempt to drive a better outcome for all involved. I enjoy working in open and honest environments rather than environments that are highly political or where there are ulterior motives.
- I will make mistakes, but I am typically quick to own them and apologize. I try to understand what I could have done better in every situation and ask the same for my team members.

## How you can help me

- Do your best, every day.
- Keep the big picture in mind - always attempt to connect your activity and output to customer value and outcomes.
- Assume positive intent from your co-workers and aim to work collaboratively with them.
- Live out the GitLab core values in everything that you do.
- Prioritize for fantastic business outcomes. We have an amazing opportunity to do what very few companies have done, so let's make the most of it!
- Have a bias for action. Don't let things sit if you can move them along. Don't wait for consensus decision making on 2 way door decisions.
- Provide me feedback on things I can do to improve. I view feedback as a requirement for growing both personally and professionally.

## Work practicals

- As I mentioned earlier, family is incredibly important to me. As such, I will typically finish my work day around 5PM central in order to maximize the amount of time I have with my children each day. In the mornings, I'm usually online around 8AM central after getting kids to school, working out, and getting ready for the day.
- I desire to be approachable, please let me know if I ever come off as opposite of that. If you need to chat outside of a scheduled 1x1, feel free to drop an ad-hoc meeting on the calendar, even if it’s the same day.
- I am typically very quick to respond to Slack/Text/Email during working hours. At times, I will turn off notifications to get deep work done, but will typically check in once an hour if I'm in that mode.
- I go back and forth between having the Slack app installed on my phone and not. If it's installed notifications are turned off.

## Eric's favorite things

Here is a list of my favorite things! I would love to discuss any or all of these with each of you.

**Books**

- [The Bible](https://www.amazon.com/Thinline-Bonded-Leather-Letter-Comfort/dp/031044876X/ref=sr_1_3?keywords=the+bible+niv&qid=1573590339&sr=8-3)
- [Inspired](https://www.amazon.com/INSPIRED-Create-Tech-Products-Customers/dp/1119387507/ref=sr_1_1?keywords=inspired+book&qid=1573590356&sr=8-1) - Marty Cagan
- [Pillars of the Earth](https://www.amazon.com/Pillars-Deluxe-Oprahs-Follett-2007-11-14/dp/B01FEKD9HQ/ref=sr_1_3?keywords=pillars+of+the+earth&qid=1573590375&sr=8-3) - Ken Follet
- [The Hard Thing About Hard Things](https://www.amazon.com/Hard-Thing-About-Things-Building/dp/B00I0A6HUO/ref=sr_1_1?crid=2VXC43S4KCW32&keywords=hard+thing+about+hard+things+book&qid=1573590391&sprefix=hard+thing+abou%2Caps%2C171&sr=8-1) - Ben Horowitz
- [Ready Player One](https://www.amazon.com/Ready-Player-One/dp/B005HG7BWC/ref=sr_1_3?keywords=ready+player+one&link_code=qs&qid=1573590547&sr=8-3) - Ernest Cline
- [Principles](https://www.amazon.com/Principles-Life-Work-Ray-Dalio/dp/1501124021) - Ray Dalio

**Video Games**

- [Final Fantasy 3](https://en.wikipedia.org/wiki/Final_Fantasy_VI)
- [Chrono Trigger](https://en.wikipedia.org/wiki/Chrono_Trigger)
- [Legend of Zelda: Breath of the Wild](https://www.amazon.com/Legend-Zelda-Breath-Wild-Nintendo-Switch/dp/B01MS6MO77/ref=sr_1_2?keywords=breath+of+the+wild&qid=1573590591&sr=8-2)
- [Super Mario Odyssey](https://www.amazon.com/Super-Mario-Odyssey-Nintendo-Switch/dp/B01MUA0D2A/ref=sr_1_2?crid=VQ06PWJDDHPP&keywords=super+mario+odyssey&qid=1573590622&sprefix=super+mario+odyssey%2Caps%2C196&sr=8-2)
- [God of War](https://en.wikipedia.org/wiki/God_of_War_%282018_video_game%29)
- [Fortnite](https://www.epicgames.com/fortnite)

Movies

- [Braveheart](https://www.amazon.com/Braveheart-Gladiator-Double-Feature-Blu-ray/dp/B06XGRB8KM/ref=sr_1_3?keywords=braveheart&qid=1573590684&sr=8-3)
- [Interstellar](https://www.amazon.com/Interstellar-Matthew-McConaughey/dp/B00TU9UFTS/ref=sr_1_1?keywords=interstellar&qid=1573590722&sr=8-1)
- [Moneyball](https://www.amazon.com/Moneyball-Brad-Pitt/dp/B006IMY5ZU/ref=sr_1_1?keywords=moneyball&qid=1573590741&sr=8-1)

**Other**

- [St. Louis Cardinals](https://www.mlb.com/cardinals)
- [University of Illinois Sports](https://fightingillini.com/)
- [Sous Vide cooking](https://anovaculinary.com/)
- [Star Wars](https://www.starwars.com/) (pretty much everything about it)
- [Legos](https://www.lego.com/en-us)
- [Playing Trumpet](https://www.facebook.com/48400275/videos/771816247375/)
- [Magic: The Gathering](https://www.moxfield.com/users/brinks618)
- [Crypto currency and mining](https://en.wikipedia.org/wiki/Cryptocurrency)
- [Camping](https://photos.app.goo.gl/unLQdCne8EpWTZM6A)
- [Beach Vacations](https://photos.app.goo.gl/DK5kSx6yAfViNeeB8)
- [Peloton bike riding](onepeloton.com) (username is ecbrinkman)
