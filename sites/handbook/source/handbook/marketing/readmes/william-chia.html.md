---
layout: markdown_page
title: "William Chia's README"
---

## William Chia README

Sr. Product Marketing Manager, Cloud Native & GitOps / Product Management Intern Monitor:Health

## Related pages

* [linkedin.com/in/williamchia/](https://www.linkedin.com/in/williamchia/)
* [twitter.com/thewilliamchia](https://twitter.com/thewilliamchia)
* [gitlab.com/williamchia](https://gitlab.com/williamchia)

## About me

* I am a big fan of the Iteration value and [Minimum Viable Change](/handbook/values/#minimal-viable-change-mvc)
* I also enjoy being meta

## Books 

These are books that have made an impact on me and I often recommend and make reference to

* [Made to Stick](https://heathbrothers.com/books/made-to-stick/)
* [Concious Business](https://www.amazon.com/Conscious-Business-Build-through-Values/dp/1622032020)
